# Competição Esportiva

De preferência com Wordpress, gostariamos de criar um site para um campeonato esportivo. O esporte é por sua escolha.

## O site deveria conter:

- Página incial com um texto e uma imagem do evento;
- Página com a lista dos competidores, listados por ordem alfabética;
- Página com detalhe de cada competidor;
- Página com ranking dos competidores, ordenados pela quantida de pontos;

## O cadastro do competidor deveria conter:

- Nome;
- Descrição;
- Imagem;
- Quantidade de pontos que ele tem na competição;

## A listagem dos competidores deveria conter:

- Nome;
- Link para exibição da imagem do competidor, abrindo em uma nova janela;
- Quantidade de pontos na competição;

## O que gostariamos de receber:

- Descrição das decisões tomadas e de como o projeto foi planejado;
- URL do projeto em funcionamento;
- Acesso a um repositório GIT público;

Boa sorte

